#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='p-minerva',
        version='1.0',
        description=('Parses PCAP files generates stats, endpoint,'
                   'and converstation list.'),
        author='Jesse Spangenberger',
        author_email='jesse.spangenberger@mymail.champlain.edu',
        license='GPL',
        url='',
        packages=find_packages(),
        install_requires = [
          "docopt",
          "scapy",
          "progressbar"
        ],
        entry_points={
            'console_scripts': [
                'p-minerva=p_minerva:main',
            ],
        },
     )
