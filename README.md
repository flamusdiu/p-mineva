# Python Mineva

This is the published script of my Week 8 project for DFS-510 at Champlain College Online. 

We were instructed to create some kind of script for digital forensics. I decided to center my script around parsing network pcap files through [scapy](https://scapy.net/).

Per the Scapy [documentation](https://scapy.readthedocs.io): "Scapy is a Python program that enables the user to send, sniff and dissect and forge network packets. This capability allows construction of tools that can probe, scan or attack networks."

This only gives follow stats:
- Total Packets 
- Total IP headers/packets
- Total TCP headers/packets
- Total UDP headers/packets
- List of source/destination ports
- List of Conversations
- List of endpoints

## Getting Started
Clone this repo and run the command supplying the pcap file. 

```
    Usage:
        p-minerva.py [-t] -f <file>
        p-minerva.py [-t] -e -f <file> [--no-header]
        p-minerva.py [-t] -c -f <file> [--no-header]
        p-minerva.py [-t] -p <type> -f <file> [--no-header]
        p-minerva.py -h | --help
        p-minerva.py --version

    Options:
        -e, --endpoints         Print all enpoints found
        -c, --conversations     Print all conversations found
        -p, --port_type <type>  Print specific port type.
        -f, --file <file>       PCAP file to parse
        -t, --time              Print parsing time to console.

        -h, --help              Shows this screen
        --version               Shows version
``

### Prerequisites

None.

### Installing

There are only a few packages to install: altgraph, docopt, scapy

```
git clone https://gitlab.com/flamusdiu/p-mineva.git
cd p-mineva
pip install .
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
