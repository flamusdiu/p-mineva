#!/usr/bin/env python

__title__   = "Python Minerva"
__version__ = "Aug 2019"

stats = {
         'total_pkts': 0,
         'total_ip': 0,
         'total_tcp': 0,
         'total_udp': 0,
         'dport': {},
         'sport': {},
         'conversations': [],
         'endpoints': []
        }

from p_minerva import run, cli, log

def main():
    run.run()
