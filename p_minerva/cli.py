from p_minerva import __version__

from docopt import docopt

def commandline_interface():
    """Python Minerva

    Usage:
        p-minerva.py [-t] -f <file>
        p-minerva.py [-t] -e -f <file> [--no-header]
        p-minerva.py [-t] -c -f <file> [--no-header]
        p-minerva.py [-t] -p <type> -f <file> [--no-header]
        p-minerva.py -h | --help
        p-minerva.py --version

    Options:
        -e, --endpoints         Print all enpoints found
        -c, --conversations     Print all conversations found
        -p, --port_type <type>  Print specific port type.
        -f, --file <file>       PCAP file to parse
        -t, --time              Print parsing time to console.

        -h, --help              Shows this screen
        --version               Shows version

    """
    class CommandLineOptions(object):
        def __repr__(self):
            """ Prints out CLI options """
            opts = {}
            for opt in self.__dict__.items():
                opts.update({opt[0]: opt[1] or None})

            return '<%s %s>' % (self.__class__.__name__, opts)

        def __setattr__(self, key, value):
            """ Substitute option names: --an-option-name for an_option_name """
            import re
            key = re.sub(r'^__', "", re.sub(r'-', "_", key))

            object.__setattr__(self, key, value)

        def __getattr__(self, item):
            """ Returns attribute from dict directly.
                Overides normal getattr()"""
            return self.__dict__[item]

    # Creats CLI options
    cli_options = CommandLineOptions()

    # Parses CLI options through docopt module.
    options = docopt(commandline_interface.__doc__.__str__(), version=__version__)

    # Sets the option using the modified CommandLineOptions.__setattr__()
    # so attributes can be called by 'options.item'
    [setattr(cli_options, opt, options[opt]) for opt in options]
    return cli_options
