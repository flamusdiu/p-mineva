from p_minerva import stats

def update_stats(pkt):
    """ Updates stats by each IP Packet """
    # Dest Ports. All ports above 1024 (non-common ports) are counted
    # together for stat purposes.
    if pkt.dport > 1024:
        stats['dport']['>1024'] = stats['dport'].get('>1024', 0) + 1
    else:
        stats['dport'][pkt.dport] = stats['dport'].get(pkt.dport, 0) + 1

    # Source Ports. All ports above 1024 (non-common ports) are counted
    # together for stat purposes.
    if pkt.sport > 1024:
        stats['sport']['>1024'] = stats['sport'].get('>1024', 0) + 1
    else:
        stats['sport'][pkt.sport] = stats['sport'].get(pkt.sport, 0) + 1

    # TCP/UDP Layer. Counts TCP/UDP per layer.
    if pkt.raw.haslayer("TCP"):
        stats['total_tcp'] = stats.get('total_tcp', 0) + 1
    elif pkt.raw.haslayer("UDP"):
        stats['total_udp'] = stats.get('total_udp', 0) + 1

    # Adds source enpoint to stats. No duplicates.
    if not pkt.src in stats['endpoints']:
        stats['endpoints'] += [pkt.src]

    # Adds dest enpoint to stats. No duplicates.
    if not pkt.dst in stats['endpoints']:
        stats['endpoints'] += [pkt.dst]

    # Adds converstaion (source, dest) to stats. No duplication
    if not pkt.conversation() in stats['conversations']:
        stats['conversations'] += [pkt.conversation()]

    # Totals all IP packets found in file.
    stats['total_ip'] = stats.get('total_ip', 0) + 1

class Packet:
    """ Packet class used to track a subset information for each packets
    """
    def __init__(self, raw):

        # Store raw packet data (unprocessed)
        self.raw = raw

        # Store and parse IP layer
        if self.raw.haslayer("IP"):
            self.version, self.src, self.dst = self.ip()

        # Store and parse TCP layer
        if self.raw.haslayer("TCP"):
            self.proto = "TCP"
            self.dport, self.sport = self.tcp()

        # Store and parse UDP layer
        elif self.raw.haslayer("UDP"):
            self.proto = "UDP"
            self.dport, self.sport = self.udp()
        else:
            self.proto = None

    def __repr__(self):
        """ Create string from information """
        return '<IP src:{} | dest:{} | proto: {}>' \
                .format(self.src, self.dst, self.proto)

    def tcp(self):
        """ Parse TCP Layer """
        raw = self.raw.getlayer("TCP")
        dport = raw.dport
        sport = raw.sport

        return [dport, sport]

    def udp(self):
        """ Parse UDP Layer """
        raw = self.raw.getlayer("UDP")
        dport = raw.dport
        sport = raw.sport

        return [dport, sport]

    def ip(self):
        """ Parse IP Layer """
        raw = self.raw.getlayer("IP")
        version = raw.version
        src = raw.src
        dst = raw.dst

        return [version, src, dst]

    def data(self):
        """ Returns data from the packet (last layer of the packet) """
        return self.raw.lastlayer if self.raw.lastlayer else None

    def conversation(self):
        """ Returns the IP packet conversation """
        return '{},{}'.format(self.src, self.dst)
