from p_minerva import __title__, __version__, stats

def print_header():
    """ Prints output header """
    print ('{title}, {version}\n'
        .format(title=__title__, version=__version__))

def print_stats():
    """ Prints all stats for PCAP file """

    print()
    print(('     Total Packets Processed: {total_pkts}\n\n'
           '                    IP Layer: {total_ip}\n'
           '                   TCP Layer: {total_tcp}\n'
           '                   UDP Layer: {total_udp}\n'
          ).format(total_pkts=stats['total_pkts'], total_ip=stats['total_ip'],
                   total_tcp=stats['total_tcp'], total_udp=stats['total_udp']))

def print_endpoints(header=True):
    """ Print out enpoint list """
    if header:
        print('Endpoints')
        print('=========\n')

    print('\n'.join(stats['endpoints']))

def print_conversations(header=True):
    """ Print out conversation list """
    if header:
        print_header()

        print('Conversations')
        print('=============\n')

    print('{:<30}{:<30}'.format('Source', 'Destination'))
    print('{:<30}{:<30}'.format('------', '-----------'))

    for c in stats['conversations']:
        print('{:<30}{:<30}'.format(*c.split(',')))

def print_ports(ports='all', header=True):
    """ Print out port list/stats """

    if ports == 'src':
        print_source_prots(stats['sport'], header)
    elif ports == 'dest':
        print_dest_ports(stats['dport'], header)
    else:
        print_source_prots(stats['sport'], header)
        print()
        print_dest_ports(stats['dport'], header)

def print_source_prots(ports, header=True):
    if header:
        print('Source Ports:')
        print('  {:<5} {:<7}'.format('Port', 'Hits'))
        print('  ----- -------')

    # Sort ports based on value
    sorted_ports = sorted([x for x in ports if type(x) is int])

    # Print each port and hit value
    for p in sorted_ports:
        print('  {:>5} {:>7}'.format(p, ports[p]))

    # Print '>1024' information last
    if '>1024' in ports:
        print('  {:>5} {:>7}'.format('>1024', ports['>1024']))

def print_dest_ports(ports, header=True):
    if header:
        print('Destination Ports:')
        print('  {:<5} {:<7}'.format('Port', 'Hits'))
        print('  ----- -------')

    # Sort ports based on value
    sorted_ports = sorted([x for x in ports if type(x) is int])

    # Print each port and hit value
    for p in sorted_ports:
        print('  {:>5} {:>7}'.format(p, ports[p]))

    # Print '>1024' information last
    if '>1024' in ports:
        print('  {:>5} {:>7}'.format('>1024', ports['>1024']))
