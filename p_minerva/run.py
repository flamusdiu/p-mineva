#! /usr/bin/env python
'''
Python Minerva
Author: Jesse Spangenberger
Description: Parses PCAP files generates stats, endpoint,
             and converstation list.
Date: 19 Aug 2019
Version: Aug 2019

Used the below PCAP file for testing.
PCAP: http://forensicscontest.com/contest09/
      spoilers/2011-Defcon-Contest-Round1/defcon2011contest-round1.html
'''

import time

from progressbar import ProgressBar

from p_minerva import log, cli, stats
from p_minerva.output import *
from p_minerva.packet import Packet as mPacket, update_stats

# Main import for p-minerva
from scapy.all import *

def read_file(file):
    """ Reads PCAP file """

    # pdpcap() provided by scapy library to read PCAP.
    print('Reading PCAP file ... this might take a while!')
    packets = rdpcap(file)

    pbar = ProgressBar()

    print('Parsing packet layers and gathering stats...')
    # Loop through all packets in the file
    for pkt in pbar(packets):
        # Only examine IP packets
        if pkt.haslayer("IP"):
            # Create packet object
            new_packet = mPacket(pkt)

            # Update packet stats
            update_stats(new_packet)

        # Counts total processed packets regardless of layer type
        stats['total_pkts'] = stats.get('total_pkts', 0) +1

    # Sort converstations
    stats['conversations'] = sorted(stats['conversations'])

    # Sort endpoints
    stats['endpoints'] = sorted(stats['endpoints'])

def run():
    """ Main function for application """
    startTime = time.time()

    # Parse the command line arguments
    args = cli.commandline_interface()

    print_header()

    # Parse PCAP file
    read_file(args.file)

    # Parse options selected
    if args.endpoints:
        print_endpoints(header = not args.no_header)
    elif args.conversations:
        print_conversations(header = not args.no_header)
    elif args.port_type:
        print_ports(args.port_type, header = not args.no_header)
    else:
        print_stats()

    # Shows processing time only if '-t' or '-time' is given
    if args.time:
        # Record the end time and calculate the duration
        endTime = time.time()
        duration = endTime - startTime

        print('Elapsed Time: ', '{:.2f}'.format(duration) + ' seconds\n')

if __name__ == '__main__':
    run()
